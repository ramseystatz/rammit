package com.rammit.util;

import java.util.ArrayList;

import com.rammit.entity.Comment;

public class Helpers {
	
	public static void printCommentTree(ArrayList<Comment> tree, int iLevel){
		
		for(int i = 0; i < tree.size(); i++){
			for(int j = 0; j < iLevel; j++)
				System.out.print("\t");
			
			String body = tree.get(i).getAuthor();
			
			if(body.length() > 20)
				System.out.print(body.substring(0,20) + "...");
			else
				System.out.print(body);
			
			System.out.print(" (" + tree.get(i).getScore() + ")");
			System.out.println();
			printCommentTree(tree.get(i).getChildComments(), iLevel + 1);
		}		
		
	}

}

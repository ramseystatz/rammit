package com.rammit.util;

public class APIEndpoint {

	// Universal constants
	public static final String BASE_URL = "http://www.reddit.com";
	public static final String API_ENDPOINT = BASE_URL + "/api";
	public static final String V1_ENDPOINT = API_ENDPOINT + "/v1";
	
	// Functions
	public static final String LOGIN = API_ENDPOINT + "/login";
	public static final String MY_SUBREDDITS = BASE_URL + "/subreddits/mine/subscriber.";
	public static final String USER = BASE_URL + "/user/";
	public static final String MY_INFO = API_ENDPOINT + "/me.json";
	
}

package com.rammit.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.rammit.entity.Comment;
import com.rammit.entity.Post;
import com.rammit.entity.User;

public class RequestFactory {
	
	public static StringBuilder login(String username, String password) {
		
		try{
			
			String url = APIEndpoint.LOGIN + "/" + username;
			HttpURLConnection con = connectionFromString(url, "POST");		
	
			String params = "user=" + username + "&passwd=" + password + "&api_type=" + RammitPreferences.RESPONSE_FORMAT;
			con.setRequestProperty("Content-Length", String.valueOf(params.length()));
			
			DataOutputStream paramWriter = new DataOutputStream(con.getOutputStream());
			paramWriter.writeBytes(params);
			paramWriter.flush();
			paramWriter.close();
			
			if(con.getResponseCode() != HttpURLConnection.HTTP_OK)
				return null;
			
			StringBuilder response = getStringBuilderFromResponse(con);
			
			return response;
			
		}catch(Exception e){
			return null;
		}
		
	}
	
	public static StringBuilder getSubscribedSubreddits(User user){
		
		try{
		
			String url = APIEndpoint.MY_SUBREDDITS + RammitPreferences.RESPONSE_FORMAT;
			HttpURLConnection con = connectionFromString(url, "GET");
			
			con.setRequestProperty("X-Modhash", user.getModhash());
			con.setRequestProperty("Cookie", "reddit_session=" + user.getCookie());
			
			if(con.getResponseCode() != HttpURLConnection.HTTP_OK)
				return null;
			
			StringBuilder response = getStringBuilderFromResponse(con);
			
			return response;
			
		}catch(Exception e){
			return null;
		}
		
	}
	
	public static StringBuilder getSubredditPosts(String subName, String sort, String timeframe){
		
		try{
			
			String url = APIEndpoint.BASE_URL + subName + sort + "." + RammitPreferences.RESPONSE_FORMAT;
			HttpURLConnection con = connectionFromString(url, "GET");
			
			if(timeframe != null){
				String params = "t=" + timeframe;
				con.setRequestProperty("Content-Length", String.valueOf(params.length()));
			
				DataOutputStream paramWriter = new DataOutputStream(con.getOutputStream());
				paramWriter.writeBytes(params);
				paramWriter.flush();
				paramWriter.close();
			}
			
			if(con.getResponseCode() != HttpURLConnection.HTTP_OK)
				return null;
			
			StringBuilder response = getStringBuilderFromResponse(con);
			
			return response;
			
		}catch(Exception e){
			return null;
		}
		
	}
	
	public static StringBuilder getUserInfo(User user){
		
		try{
			
			String url = "";
			if(user.getModhash() == null)
				url = APIEndpoint.USER + user.getUsername() + "/about.json";
			else
				url = APIEndpoint.MY_INFO;
			
			//System.out.println(url);
			
			HttpURLConnection con = connectionFromString(url, "GET");
			
			if(user.getModhash() != null)
				con.setRequestProperty("X-Modhash", user.getModhash());
				con.setRequestProperty("Cookie", "reddit_session=" + user.getCookie());
			
			if(con.getResponseCode() != HttpURLConnection.HTTP_OK)
				return null;
			
			StringBuilder response = getStringBuilderFromResponse(con);
			
			return response;
			
		}catch(Exception e){
			return null;
		}
		
	}
	
	public static StringBuilder getUserPosts(User user){
		
		try{
			
			String url = APIEndpoint.USER + user.getUsername() + "/submitted.json";
			
			//System.out.println(url);
			
			HttpURLConnection con = connectionFromString(url, "GET");
			
			if(con.getResponseCode() != HttpURLConnection.HTTP_OK)
				return null;
			
			StringBuilder response = getStringBuilderFromResponse(con);
			
			return response;
			
		}catch(Exception e){
			return null;
		}
		
	}
	
	public static StringBuilder getUserComments(User user){
		
		try{
			
			String url = APIEndpoint.USER + user.getUsername() + "/comments.json";
			
			//System.out.println(url);
			
			HttpURLConnection con = connectionFromString(url, "GET");
			
			if(con.getResponseCode() != HttpURLConnection.HTTP_OK)
				return null;
			
			StringBuilder response = getStringBuilderFromResponse(con);
			
			return response;
			
		}catch(Exception e){
			return null;
		}
		
	}
	
	public static StringBuilder getUserFriends(User user){
		
		try{
			
			String url = APIEndpoint.V1_ENDPOINT + "/me/friends.json";
			
			HttpURLConnection con = connectionFromString(url, "GET");
			
			if(user.getModhash() != null)
				con.setRequestProperty("X-Modhash", user.getModhash());
				con.setRequestProperty("Cookie", "reddit_session=" + user.getCookie());
			
			if(con.getResponseCode() != HttpURLConnection.HTTP_OK)
				return null;
			
			StringBuilder response = getStringBuilderFromResponse(con);
			
			return response;
			
		}catch(Exception e){
			return null;
		}
		
	}
	
	public static StringBuilder getPostComments(Post post, String sort){
		
		try{
			
			String url = APIEndpoint.BASE_URL + "/r/" + post.getSubreddit() + "/comments/" + post.getId() + ".json";
			//System.out.println(url);
			
			HttpURLConnection con = connectionFromString(url, "GET");
			
			if(sort != null){
				String params = "sort=" + sort;
				con.setRequestProperty("Content-Length", String.valueOf(params.length()));
			
				DataOutputStream paramWriter = new DataOutputStream(con.getOutputStream());
				paramWriter.writeBytes(params);
				paramWriter.flush();
				paramWriter.close();
			}
			
			if(con.getResponseCode() != HttpURLConnection.HTTP_OK)
				return null;
			
			StringBuilder response = getStringBuilderFromResponse(con);
			
			return response;
			
		}catch(Exception e){
			return null;
		}
		
	}
	
	public static StringBuilder upvote(User user, Comment comment){
		return vote(user, comment, "1");
	}
	
	public static StringBuilder downvote(User user, Comment comment){
		return vote(user, comment, "-1");
	}
	
	public static StringBuilder unvote(User user, Comment comment){
		return vote(user, comment, "0");
	}
	
	public static StringBuilder vote(User user, Comment comment, String dir){
		
		try{
			
			String url = APIEndpoint.API_ENDPOINT + "/vote";	
			
			HttpURLConnection con = connectionFromString(url, "POST");
			
			if(user.getModhash() != null)
				con.setRequestProperty("X-Modhash", user.getModhash());
				con.setRequestProperty("Cookie", "reddit_session=" + user.getCookie());
			
			String params = "id=t1_" + comment.getId() + "&dir=" + dir;
			con.setRequestProperty("Content-Length", String.valueOf(params.length()));
		
			DataOutputStream paramWriter = new DataOutputStream(con.getOutputStream());
			paramWriter.writeBytes(params);
			paramWriter.flush();
			paramWriter.close();
			
			if(con.getResponseCode() != HttpURLConnection.HTTP_OK)
				return null;
			
			StringBuilder response = getStringBuilderFromResponse(con);
			
			return response;
			
		}catch(Exception e){
			return null;
		}
		
	}
	
	public static StringBuilder upvote(User user, Post post){
		return vote(user, post, "1");
	}
	
	public static StringBuilder downvote(User user, Post post){
		return vote(user, post, "-1");
	}
	
	public static StringBuilder unvote(User user, Post post){
		return vote(user, post, "0");
	}
	
	public static StringBuilder vote(User user, Post post, String dir){
		
		try{
			
			String url = APIEndpoint.API_ENDPOINT + "/vote";	
			
			HttpURLConnection con = connectionFromString(url, "POST");
			
			if(user.getModhash() != null)
				con.setRequestProperty("X-Modhash", user.getModhash());
				con.setRequestProperty("Cookie", "reddit_session=" + user.getCookie());
			
			String params = "id=t3_" + post.getId() + "&dir=" + dir;
			con.setRequestProperty("Content-Length", String.valueOf(params.length()));
		
			DataOutputStream paramWriter = new DataOutputStream(con.getOutputStream());
			paramWriter.writeBytes(params);
			paramWriter.flush();
			paramWriter.close();
			
			if(con.getResponseCode() != HttpURLConnection.HTTP_OK)
				return null;
			
			StringBuilder response = getStringBuilderFromResponse(con);
			
			return response;
			
		}catch(Exception e){
			return null;
		}
		
	}
	
	
	/*
	 * Does things that need to be performed for every HTTP request
	 * setUserAgent, open the connection ,etc.
	 */
	public static HttpURLConnection connectionFromString(String url, String method) {
		
		try{
			HttpURLConnection req = (HttpURLConnection)(new URL(url).openConnection());
			req.setRequestMethod(method);
			req.setDoOutput(true);
			req.setRequestProperty("User-Agent", RammitPreferences.USER_AGENT);
			return req;
		}catch(Exception e){
			return null;
		}
	}
	
	public static StringBuilder getStringBuilderFromResponse(HttpURLConnection request){
		
		try{
			StringBuilder response = new StringBuilder();
	        BufferedReader input = new BufferedReader(new InputStreamReader(request.getInputStream()), 8192);
	        String strLine = null;
	        while ((strLine = input.readLine()) != null)
	        {
	            response.append(strLine);
	        }
	        input.close();
	        return response;
		}catch(Exception e){
			System.out.println("Error getting response from server.");
		}
		
		return null;
		
	}

}

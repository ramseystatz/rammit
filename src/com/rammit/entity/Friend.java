package com.rammit.entity;

import org.json.simple.JSONObject;

public class Friend {
	
	private String date;
	private String name;
	private String id;
	
	public Friend(JSONObject friend){
		this.date = friend.get("date") == null ? "null" : friend.get("date").toString();
		this.name = friend.get("name") == null ? "null" : friend.get("name").toString();
		this.id = friend.get("id") == null ? "null" : friend.get("id").toString();
	}
	
	public Friend(String name){
		this.name = name;
	}
	
	@Override
	public boolean equals(Object friend){
		
		if(getClass() != friend.getClass())
			return false;
		
		return this.name.equals(((Friend)friend).getName());
	}

	public String getDate() {
		return date;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}
	
	@Override
	public String toString(){
		return "Date: " + this.date + "\n" +
				"Name: " + this.name + "\n" +
				"ID: " + this.id + "\n";
	}

}

package com.rammit.entity;

import org.json.simple.JSONObject;

public class UserInfo {
	
	private Boolean isLoggedIn;
	
	// Ubiquitous properties
	private String username;
	private String isFriend;
	private String created;
	private String linkKarma;
	private String commentKarma;
	private String isGold;
	private String isMod;
	private String hasVerifiedEmail;
	
	// Logged-in-specific properties
	private String hasMail;
	private String goldCredits;
	private String over18;
	private String goldExpiration; // Not used due to weird error ("null"?)
	private String hasModMail;
	
	// NOTE: Rewrite these getter and setters to
	// return with types that make sense(ints for karma, etc)
	

	// Ubiquitous getters and setters
	public boolean isLoggedIn(){
		return isLoggedIn;
	}
	
	public String getUsername() {
		return username;
	}

	public boolean getIsFriend() {
		return isFriend.equals("true");
	}

	public float getCreated() {
		return Float.parseFloat(created);
	}

	public int getLinkKarma() {
		return Integer.parseInt(linkKarma);
	}

	public int getCommentKarma() {
		return Integer.parseInt(commentKarma);
	}

	public boolean getIsGold() {
		return isGold.equals("true");
	}

	public boolean getIsMod() {
		return isMod.equals("true");
	}

	public boolean getHasVerifiedEmail() {
		return hasVerifiedEmail.equals("true");
	}
	
	// Logged-in-specific getters and setters
	
	public boolean getHasMail() {
		
		if(!isLoggedIn)
			return false;
			//return UserNotLoggedInException();
		
		return hasMail.equals("true");
	}

	public int getGoldCredits() {
		
		if(!isLoggedIn)
			return 0;
			//return UserNotLoggedInException();
		
		return Integer.parseInt(goldCredits);
	}

	public boolean getOver18() {
		
		if(!isLoggedIn)
			return false;
			//return UserNotLoggedInException();
		
		return over18.equals("true");
	}

	public boolean getHasModMail() {
		
		if(!isLoggedIn)
			return false;
			//return UserNotLoggedInException();
		
		return hasModMail.equals("true");
	}

	public UserInfo(JSONObject info){
		
		if(info.containsKey("has_mod_mail"))
			this.isLoggedIn = true;
		else
			this.isLoggedIn = false;
		
		this.username = info.get("name").toString();
		this.isFriend = info.get("is_friend").toString();
		this.created = info.get("created").toString();
		this.linkKarma = info.get("link_karma").toString();
		this.commentKarma = info.get("comment_karma").toString();
		this.isGold = info.get("is_gold").toString();
		this.isMod = info.get("is_mod").toString();
		this.hasVerifiedEmail = info.get("has_verified_email").toString();
		
		if(this.isLoggedIn){
    		this.hasMail = info.get("has_mail").toString();
    		this.goldCredits = info.get("gold_creddits").toString();
    		this.over18 = info.get("over_18").toString();
    		this.hasModMail = info.get("has_mod_mail").toString();
		}
		
	}
	
	@Override
	public String toString(){
		
		String s = "Username: " + username + "\n";
		s += "isFriend: " + isFriend + "\n";
		s += "created: " + created + "\n";
		s += "linkKarma: " + linkKarma + "\n";
		s += "commentKarma: " + commentKarma + "\n";
		s += "isGold: " + isGold + "\n";
		s += "isMod: " + isMod + "\n";
		s += "hasVerifiedEmail: " + hasVerifiedEmail + "\n";
		
		if(isLoggedIn()){
			s += "hasMail: " + hasMail + "\n";
			s += "goldCredits: " + goldCredits + "\n";
			s += "over18: " + over18 + "\n";
			s += "hasModMail: " + hasModMail + "\n";
		}
		
		return s;
	}

}

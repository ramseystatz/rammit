package com.rammit.entity;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.rammit.util.RequestFactory;

public class User {
	
	// User info
	private String username;
	private String password;
	private String modhash;
	private String cookie;
	
	public User(String username){
		this.username = username;
		this.password = "";
	}
	
	public User(String username, String password){
		this.username = username;
		this.password = password;		
	}
	
	public String getModhash(){
		return this.modhash;
	}
	
	public String getCookie(){
		return this.cookie;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public void connect(){
		
		try {           
			
			StringBuilder response = RequestFactory.login(username, password);
			
			if(response == null)
				throw new Exception();
	        
	        JSONObject loginResponse = (JSONObject)new JSONParser().parse(response.toString());
	        loginResponse = (JSONObject)new JSONParser().parse(loginResponse.get("json").toString());
	        loginResponse = (JSONObject)new JSONParser().parse(loginResponse.get("data").toString());
	        
	        // Get modhash and cookie
	        this.modhash = (loginResponse.get("modhash").toString());
	        this.cookie = (loginResponse.get("cookie").toString());
			
	    } catch (Exception e) {
	    	System.out.println("Unable to connect User: " + username + "//" + password);
	    }
	}

	
	/*
	 * Issues
	 * 
	 * Only gets first 25 subreddits (default behavior)
	 */
	public ArrayList<Subreddit> getSubscribedSubreddits(){
		
		try{
			
			StringBuilder response = RequestFactory.getSubscribedSubreddits(this);
					
			if(response == null)
				throw new Exception();
	        
	        JSONObject userSubredditResponse = (JSONObject)new JSONParser().parse(response.toString());
	        userSubredditResponse = (JSONObject)new JSONParser().parse(userSubredditResponse.get("data").toString());
	        JSONArray subreddits = (JSONArray)new JSONParser().parse(userSubredditResponse.get("children").toString());
	        
	        Iterator<JSONObject> i = subreddits.iterator();
	        ArrayList<Subreddit> reddits = new ArrayList<Subreddit>();
	        
	        while(i.hasNext()){
	        	JSONObject currSub = (JSONObject)i.next();
	        	currSub = (JSONObject)new JSONParser().parse(currSub.get("data").toString());
	        	reddits.add(new Subreddit(currSub));
	        }
	        
	        return reddits;	        
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Unable to retrieve list of subreddits for /u/" + username);
		}
		
		return null;
		
	}
	
	public UserInfo getUserInfo(){
		
		try{
			
			StringBuilder response = RequestFactory.getUserInfo(this);
			
			if(response == null)
				throw new Exception();
			
			JSONObject userInfoResponse = (JSONObject)new JSONParser().parse(response.toString());
			//System.out.println(response.toString());
	        userInfoResponse = (JSONObject)new JSONParser().parse(userInfoResponse.get("data").toString());
	        
	        return new UserInfo(userInfoResponse);
			
		}catch(Exception e){
			//e.printStackTrace();
			System.out.println("Unable to retrieve user info for /u/" + username);
		}
		
		return null;
		
	}
	
	public ArrayList<Post> getUserPosts(){
		
		try{
			
			StringBuilder response = RequestFactory.getUserPosts(this);
			
			if(response == null)
				throw new Exception();
			
			JSONObject userPostsResponse = (JSONObject)new JSONParser().parse(response.toString());
			//System.out.println(response.toString());
			userPostsResponse = (JSONObject)new JSONParser().parse(userPostsResponse.get("data").toString());
	        JSONArray posts = (JSONArray)new JSONParser().parse(userPostsResponse.get("children").toString());
			
	        Iterator<JSONObject> i = posts.iterator();
	        ArrayList<Post> subPosts = new ArrayList<Post>();
	        
	        while(i.hasNext()){
	        	JSONObject currPost = (JSONObject)i.next();
	        	currPost = (JSONObject)new JSONParser().parse(currPost.get("data").toString());
	        	subPosts.add(new Post(currPost));
	        }
	        
	        return subPosts;
			
		}catch(Exception e){
			System.out.println("Unable to retrieve user posts for /u/" + username);
		}
		
		return null;
		
	}
	
	public ArrayList<Comment> getUserComments(){
		
		try{
			
			StringBuilder response = RequestFactory.getUserComments(this);
			
			if(response == null)
				throw new Exception();
			
			JSONObject userCommentsResponse = (JSONObject)new JSONParser().parse(response.toString());
			//System.out.println(response.toString());
			userCommentsResponse = (JSONObject)new JSONParser().parse(userCommentsResponse.get("data").toString());
	        JSONArray comments = (JSONArray)new JSONParser().parse(userCommentsResponse.get("children").toString());
			
	        Iterator<JSONObject> i = comments.iterator();
	        ArrayList<Comment> userComments = new ArrayList<Comment>();
	        
	        while(i.hasNext()){
	        	JSONObject currComment = (JSONObject)i.next();
	        	currComment = (JSONObject)new JSONParser().parse(currComment.get("data").toString());
	        	userComments.add(new Comment(currComment));
	        }
	        
	        return userComments;
			
		}catch(Exception e){
			System.out.println("Unable to retrieve comments for /u/" + username);
		}
		
		return null;
		
	}
	
	public ArrayList<Friend> getUserFriends(){
		
		try{
			
			StringBuilder response = RequestFactory.getUserFriends(this);
			
			if(response == null)
				throw new Exception();
			
			JSONObject userFriendsResponse = (JSONObject)new JSONParser().parse(response.toString());
			//System.out.println(response.toString());
			userFriendsResponse = (JSONObject)new JSONParser().parse(userFriendsResponse.get("data").toString());
	        JSONArray friends = (JSONArray)new JSONParser().parse(userFriendsResponse.get("children").toString());
	        
	        Iterator<JSONObject> i = friends.iterator();
	        ArrayList<Friend> userFriends = new ArrayList<Friend>();
	        
	        while(i.hasNext()){
	        	JSONObject currFriend = (JSONObject)i.next();
	        	userFriends.add(new Friend(currFriend));
	        }
	        
	        return userFriends;
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Unable to retrieve friends for /u/" + username);
		}
		
		return null;
		
	}
	
	public void upvote(Comment comment){
		
		try {           
			
			StringBuilder response = RequestFactory.upvote(this, comment);
			
			if(response == null)
				throw new Exception();
			
			//System.out.println(response.toString());
			
	    } catch (Exception e) {
	    	System.out.println("Unable to upvote comment");
	    }		
		
	}
	
	public void downvote(Comment comment){
		
		try {           
			
			StringBuilder response = RequestFactory.downvote(this, comment);
			
			if(response == null)
				throw new Exception();
			
			//System.out.println(response.toString());
			
	    } catch (Exception e) {
	    	System.out.println("Unable to downvote comment");
	    }
		
	}
	
	public void unvote(Comment comment){
		
		try {           
			
			StringBuilder response = RequestFactory.unvote(this, comment);
			
			if(response == null)
				throw new Exception();
			
			//System.out.println(response.toString());
			
	    } catch (Exception e) {
	    	System.out.println("Unable to downvote comment");
	    }
		
	}
	
	public void upvote(Post post){
		
		try {           
			
			StringBuilder response = RequestFactory.upvote(this, post);
			
			if(response == null)
				throw new Exception();
			
			System.out.println(response.toString());
			
	    } catch (Exception e) {
	    	System.out.println("Unable to upvote post");
	    }
		
	}
	
	public void downvote(Post post){
		
		try {           
			
			StringBuilder response = RequestFactory.downvote(this, post);
			
			if(response == null)
				throw new Exception();
			
			System.out.println(response.toString());
			
	    } catch (Exception e) {
	    	System.out.println("Unable to downvote post");
	    }
		
	}
	
	public void unvote(Post post){
		
		try {           
			
			StringBuilder response = RequestFactory.unvote(this, post);
			
			if(response == null)
				throw new Exception();
			
			System.out.println(response.toString());
			
	    } catch (Exception e) {
	    	System.out.println("Unable to downvote post");
	    }
		
	}
	
}

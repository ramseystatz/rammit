package com.rammit.entity;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Comment {
	
	private String subredditID;
	private String linkTitle;
	private String bannedBy;
	private String subreddit;
	private String linkAuthor;
	private String likes;
	private String replies;
	private String saved;
	private String id;
	private String gilded;
	private String author;
	private String parentID;
	private String score;
	private String approvedBy;
	private String controversiality;
	private String body;
	private String edited;
	private String authorFlairCSSClass;
	private String downs;
	private String bodyHTML;
	private String linkID;
	private String scoreHidden;
	private String name;
	private String created;
	private String authorFlairText;
	private String linkURL;
	private String createdUTC;
	private String distinguished;
	private String numReports;
	private String ups;
	
	private ArrayList<Comment> childComments;
	
	public Comment(JSONObject comment){
		this.subredditID = comment.get("subreddit_id") == null ? "null" : comment.get("subreddit_id").toString();
		this.linkTitle = comment.get("link_title") == null ? "null" : comment.get("link_title").toString();
		this.bannedBy = comment.get("banned_by") == null ? "null" : comment.get("banned_by").toString();
		this.subreddit = comment.get("subreddit") == null ? "null" : comment.get("subreddit").toString();
		this.linkAuthor = comment.get("link_author") == null ? "null" : comment.get("link_author").toString();
		this.likes = comment.get("likes") == null ? "null" : comment.get("likes").toString();
		this.replies = comment.get("replies") == null ? "null" : comment.get("replies").toString();
		this.saved = comment.get("saved") == null ? "null" : comment.get("saved").toString();
		this.id = comment.get("id") == null ? "null" : comment.get("id").toString();
		this.gilded = comment.get("gilded") == null ? "null" : comment.get("gilded").toString();
		this.author = comment.get("author") == null ? "null" : comment.get("author").toString();
		this.parentID = comment.get("parent_id") == null ? "null" : comment.get("parent_id").toString();
		this.score = comment.get("score") == null ? "null" : comment.get("score").toString();
		this.approvedBy = comment.get("approved_by") == null ? "null" : comment.get("approved_by").toString();
		this.controversiality = comment.get("controversiality") == null ? "null" : comment.get("controversiality").toString();
		this.body = comment.get("body") == null ? "null" : comment.get("body").toString();
		this.edited = comment.get("edited") == null ? "null" : comment.get("edited").toString();
		this.authorFlairCSSClass = comment.get("author_flair_css_class") == null ? "null" : comment.get("author_flair_css_class").toString();
		this.downs = comment.get("downs") == null ? "null" : comment.get("downs").toString();
		this.bodyHTML = comment.get("body_html") == null ? "null" : comment.get("body_html").toString();
		this.linkID = comment.get("link_id") == null ? "null" : comment.get("link_id").toString();
		this.scoreHidden = comment.get("score_hidden") == null ? "null" : comment.get("score_hidden").toString();
		this.name = comment.get("name") == null ? "null" : comment.get("name").toString();
		this.created = comment.get("created") == null ? "null" : comment.get("created").toString();
		this.authorFlairText = comment.get("author_flair_text") == null ? "null" : comment.get("author_flair_text").toString();
		this.linkURL = comment.get("link_url") == null ? "null" : comment.get("link_url").toString();
		this.createdUTC = comment.get("created_utc") == null ? "null" : comment.get("created_utc").toString();
		this.distinguished = comment.get("distinguished") == null ? "null" : comment.get("distinguished").toString();
		this.numReports = comment.get("num_reports") == null ? "null" : comment.get("num_reports").toString();
		this.ups = comment.get("ups") == null ? "null" : comment.get("ups").toString();
		
		this.childComments = parseChildComments();
	}
	
	public Comment(String text){
		this.body = text;
	}
	
	@Override
	public boolean equals(Object comment){
		
		if(getClass() != comment.getClass())
			return false;
		
		return this.body.equals(((Comment)comment).getBody());
	}
	
	public String getSubredditID() {
		return subredditID;
	}

	public String getLinkTitle() {
		return linkTitle;
	}

	public String getBannedBy() {
		return bannedBy;
	}

	public String getSubreddit() {
		return subreddit;
	}

	public String getLinkAuthor() {
		return linkAuthor;
	}

	public String getLikes() {
		return likes;
	}

	public String getReplies() {
		return replies;
	}

	public boolean getSaved() {
		return saved.equals("true");
	}

	public String getId() {
		return id;
	}

	public int getGilded() {
		return Integer.parseInt(gilded);
	}

	public String getAuthor() {
		return author;
	}

	public String getParentID() {
		return parentID;
	}

	public int getScore() {
		return Integer.parseInt(score);
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public int getControversiality() {
		return Integer.parseInt(controversiality);
	}

	public String getBody() {
		return body;
	}

	public boolean getEdited() {
		return edited.equals("true");
	}

	public String getAuthorFlairCSSClass() {
		return authorFlairCSSClass;
	}

	public int getDowns() {
		return Integer.parseInt(downs);
	}

	public String getBodyHTML() {
		return bodyHTML;
	}

	public String getLinkID() {
		return linkID;
	}

	public String getScoreHidden() {
		return scoreHidden;
	}

	public String getName() {
		return name;
	}

	public String getCreated() {
		return created;
	}

	public String getAuthorFlairText() {
		return authorFlairText;
	}

	public String getLinkURL() {
		return linkURL;
	}

	public String getCreatedUTC() {
		return createdUTC;
	}

	public String getDistinguished() {
		return distinguished;
	}

	public String getNumReports() {
		return numReports;
	}

	public int getUps() {
		return Integer.parseInt(ups);
	}

	public String getText(){
		return this.body;
	}

	public ArrayList<Comment> getChildComments() {
		return childComments;
	}
	
	private ArrayList<Comment> parseChildComments(){
		
		if(this.getReplies().equals("")){
			return new ArrayList<Comment>();
		}
		
		try{
		
			ArrayList<Comment> comments = new ArrayList<Comment>();
			
			JSONObject commentReplies = (JSONObject)new JSONParser().parse(this.getReplies());
			//System.out.println(this.getId());
			commentReplies = (JSONObject)new JSONParser().parse(commentReplies.get("data").toString());
	        JSONArray repliesArray = (JSONArray)new JSONParser().parse(commentReplies.get("children").toString());
	        
	        Iterator<JSONObject> i = repliesArray.iterator();
	        
	        while(i.hasNext()){
	        	JSONObject currReply = (JSONObject)i.next();
	        	
	        	if(currReply.get("kind").toString().equals("t1")){
	        		currReply = (JSONObject)new JSONParser().parse(currReply.get("data").toString());
	        		comments.add(new Comment(currReply));
	        	}
	        }
	        
	        return comments;
	        
		}catch(Exception e){
			e.printStackTrace();
        	System.out.println("Unable to parse child comments");
        }
		
		return new ArrayList<Comment>();
		
	}
	
	@Override
	public String toString(){
		return "Subreddit ID: " + this.subredditID + "\n" +
				"Link title: " + this.linkTitle + "\n" +
				"Banned by: " + this.bannedBy + "\n" +
				"Subreddit: " + this.subreddit + "\n" +
				"Link author: " + this.linkAuthor + "\n" +
				"Likes: " + this.likes + "\n" +
				"Replies: " + this.replies + "\n" +
				"Saved: " + this.saved + "\n" +
				"ID: " + this.id + "\n" +
				"Gilded: " + this.gilded + "\n" +
				"Author: " + this.author + "\n" +
				"Parent ID: " + this.parentID + "\n" +
				"Score: " + this.score + "\n" +
				"Approved by: " + this.approvedBy + "\n" +
				"Controversiality: " + this.controversiality + "\n" +
				"Body: " + this.body + "\n" +
				"Edited: " + this.edited + "\n" +
				"Author flair CSS class: " + this.authorFlairCSSClass + "\n" +
				"Downs: " + this.downs + "\n" +
				"Body HTML: " + this.bodyHTML + "\n" +
				"Link ID: " + this.linkID + "\n" +
				"Score hidden: " + this.scoreHidden + "\n" +
				"Name: " + this.name + "\n" +
				"Created: " + this.created + "\n" +
				"Author flair text: " + this.authorFlairText + "\n" +
				"Link URL: " + this.linkURL + "\n" +
				"Created UTC: " + this.createdUTC + "\n" +
				"Distinguished: " + this.distinguished + "\n" +
				"Num reports: " + this.numReports + "\n" +
				"Ups: " + this.ups + "\n";
	}

}

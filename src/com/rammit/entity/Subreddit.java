package com.rammit.entity;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.rammit.util.RequestFactory;
import com.rammit.util.Sort;

public class Subreddit {
	
	private String submitTextHTML;
	private String userIsBanned;
	private String id;
	private String submitText;
	private String displayName;
	private String headerImg;
	private String descriptionHTML;
	private String title;
	private String over18;
	private String publicDescriptionHTML;
	private String userIsContributor;
	private String headerTitle;
	private String description;
	private String submitLinkLabel;
	private String accountsActive;
	private String publicTraffic;
	private String headerSize;
	private String subscribers;
	private String submitTextLabel;
	private String name;
	private String created;
	private String url;
	private String createdUTC;
	private String userIsModerator;
	private String publicDescription;
	private String commentScoreHideMins;
	private String subredditType;
	private String submissionType;
	private String userIsSubscriber;
	
	public Subreddit(JSONObject sub){
		this.submitTextHTML = sub.get("submit_text_html") == null ? "null" : sub.get("submit_text_html").toString();
		this.userIsBanned = sub.get("user_is_banned") == null ? "null" : sub.get("user_is_banned").toString();
		this.id = sub.get("id") == null ? "null" : sub.get("id").toString();
		this.submitText = sub.get("submit_text") == null ? "null" : sub.get("submit_text").toString();
		this.displayName = sub.get("display_name") == null ? "null" : sub.get("display_name").toString();
		this.headerImg = sub.get("header_img") == null ? "null" : sub.get("header_img").toString();
		this.descriptionHTML = sub.get("description_html") == null ? "null" : sub.get("description_html").toString();
		this.title = sub.get("title") == null ? "null" : sub.get("title").toString();
		this.over18 = sub.get("over18") == null ? "null" : sub.get("over18").toString();
		this.publicDescriptionHTML = sub.get("public_description_html") == null ? "null" : sub.get("public_description_html").toString();
		this.userIsContributor = sub.get("user_is_contributor") == null ? "null" : sub.get("user_is_contributor").toString();
		this.headerTitle = sub.get("header_title") == null ? "null" : sub.get("header_title").toString();
		this.description = sub.get("description") == null ? "null" : sub.get("description").toString();
		this.submitLinkLabel = sub.get("submit_link_label") == null ? "null" : sub.get("submit_link_label").toString();
		this.accountsActive = sub.get("accounts_active") == null ? "null" : sub.get("accounts_active").toString();
		this.publicTraffic = sub.get("public_traffic") == null ? "null" : sub.get("public_traffic").toString();
		this.headerSize = sub.get("header_size") == null ? "null" : sub.get("header_size").toString();
		this.subscribers = sub.get("subscribers") == null ? "null" : sub.get("subscribers").toString();
		this.submitTextLabel = sub.get("submit_text_label") == null ? "null" : sub.get("submit_text_label").toString();
		this.name = sub.get("name") == null ? "null" : sub.get("name").toString();
		this.created = sub.get("created") == null ? "null" : sub.get("created").toString();
		this.url = sub.get("url") == null ? "null" : sub.get("url").toString();
		this.createdUTC = sub.get("created_utc") == null ? "null" : sub.get("created_utc").toString();
		this.userIsModerator = sub.get("user_is_moderator") == null ? "null" : sub.get("user_is_moderator").toString();
		this.publicDescription = sub.get("public_description") == null ? "null" : sub.get("public_description").toString();
		this.commentScoreHideMins = sub.get("sub_score_hide_mins") == null ? "null" : sub.get("sub_score_hide_mins").toString();
		this.subredditType = sub.get("subreddit_type") == null ? "null" : sub.get("subreddit_type").toString();
		this.submissionType = sub.get("submission_type") == null ? "null" : sub.get("submission_type").toString();
		this.userIsSubscriber = sub.get("user_is_subscriber") == null ? "null" : sub.get("user_is_subscriber").toString();
	}
	
	public String getSubmitTextHTML() {
		return submitTextHTML;
	}

	public boolean getUserIsBanned() {
		return userIsBanned.equals("true");
	}

	public String getID() {
		return id;
	}

	public String getSubmitText() {
		return submitText;
	}

	public String getDisplayName() {
		return displayName;
	}

	public String getHeaderImg() {
		return headerImg;
	}

	public String getDescriptionHTML() {
		return descriptionHTML;
	}

	public String getTitle() {
		return title;
	}

	public boolean getOver18() {
		return over18.equals("true");
	}

	public String getPublicDescriptionHTML() {
		return publicDescriptionHTML;
	}

	public boolean getUserIsContributor() {
		return userIsContributor.equals("true");
	}

	public String getHeaderTitle() {
		return headerTitle;
	}

	public String getDescription() {
		return description;
	}

	public String getSubmitLinkLabel() {
		return submitLinkLabel;
	}

	public int getAccountsActive() {
		return Integer.parseInt(accountsActive);
	}

	public boolean getPublicTraffic() {
		return publicTraffic.equals("true");
	}

	public String getHeaderSize() {
		return headerSize;
	}

	public int getSubscribers() {
		return Integer.parseInt(subscribers);
	}

	public String getSubmitTextLabel() {
		return submitTextLabel;
	}

	public String getCreated() {
		return created;
	}

	public String getUrl() {
		return url;
	}

	public String getCreatedUTC() {
		return createdUTC;
	}

	public boolean getUserIsModerator() {
		return userIsModerator.equals("true");
	}

	public String getPublicDescription() {
		return publicDescription;
	}

	public String getCommentScoreHideMins() {
		return commentScoreHideMins;
	}

	public String getSubredditType() {
		return subredditType;
	}

	public String getSubmissionType() {
		return submissionType;
	}

	public boolean getUserIsSubscriber() {
		return userIsSubscriber.equals("true");
	}

	public Subreddit(String url){
		this.url = url;
	}
	
	public String getName(){
		return name;
	}
	
	@Override
	public boolean equals(Object sub){
		
		if(getClass() != sub.getClass())
			return false;
		
		return this.url.equals(((Subreddit)sub).url);
	}
	
	public ArrayList<Post> getPosts(){
		return getPosts(Sort.HOT, null);
	}
	
	public ArrayList<Post> getPosts(String sort){
		return getPosts(sort, null);
	}
	
	public ArrayList<Post> getPosts(String sort, String timeframe){
		
		try{
			
			StringBuilder response = RequestFactory.getSubredditPosts(this.url, sort, timeframe);
			
			if(response == null)
				throw new Exception();
	        
	        JSONObject subredditPostsResponse = (JSONObject)new JSONParser().parse(response.toString());
	        subredditPostsResponse = (JSONObject)new JSONParser().parse(subredditPostsResponse.get("data").toString());
	        JSONArray posts = (JSONArray)new JSONParser().parse(subredditPostsResponse.get("children").toString());
	        
	        Iterator<JSONObject> i = posts.iterator();
	        ArrayList<Post> subPosts = new ArrayList<Post>();
	        
	        while(i.hasNext()){
	        	JSONObject currPost = (JSONObject)i.next();
	        	currPost = (JSONObject)new JSONParser().parse(currPost.get("data").toString());
	        	subPosts.add(new Post(currPost));
	        }
	        
	        return subPosts;
	        
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Unable to retrieve list of posts for " + this.name);
		}
		
		return null;        
		
	}
	
	@Override
	public String toString(){
		return "Submit text HTML: " + this.submitTextHTML + "\n" +
				"User is banned: " + this.userIsBanned + "\n" +
				"ID: " + this.id + "\n" +
				"Submit text: " + this.submitText + "\n" +
				"Display name: " + this.displayName + "\n" +
				"Header image: " + this.headerImg + "\n" +
				"Description HTML: " + this.descriptionHTML + "\n" +
				"Title: " + this.title + "\n" +
				"Over 18: " + this.over18 + "\n" +
				"Public description HTML: " + this.publicDescriptionHTML + "\n" +
				"User is contributor: " + this.userIsContributor + "\n" +
				"Header title: " + this.headerTitle + "\n" +
				"Description: " + this.description + "\n" +
				"Submit link label: " + this.submitLinkLabel + "\n" +
				"Accounts active: " + this.accountsActive + "\n" +
				"Public traffic: " + this.publicTraffic + "\n" +
				"Header size: " + this.headerSize + "\n" +
				"Subscribers: " + this.subscribers + "\n" +
				"Submit text label: " + this.submitTextLabel + "\n" +
				"Name: " + this.name + "\n" +
				"Created: " + this.created + "\n" +
				"URL: " + this.url + "\n" +
				"Created UTC: " + this.createdUTC + "\n" +
				"User is moderator" + this.userIsModerator + "\n" +
				"Public description: " + this.publicDescription + "\n" +
				"Comment score hide mins: " + this.commentScoreHideMins + "\n" +
				"Subreddit type: " + this.subredditType + "\n" +
				"Submission type: " + this.submissionType + "\n" +
				"User is subscriber: " + this.userIsSubscriber + "\n";
	}

}

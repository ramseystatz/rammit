package com.rammit.entity;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.rammit.util.RequestFactory;
import com.rammit.util.Sort;

public class Post {
	
	// All the fields in a post JSON object, in order
	
	public String getDomain() {
		return domain;
	}

	public String getBannedBy() {
		return bannedBy;
	}

	public String getMediaEmbed() {
		return mediaEmbed;
	}

	public String getSubreddit() {
		return subreddit;
	}

	public String getSelfTextHTML() {
		return selfTextHTML;
	}

	public String getSelfText() {
		return selfText;
	}

	public String getLikes() {
		return likes;
	}

	public String getSecureMedia() {
		return secureMedia;
	}

	public String getLinkFlairText() {
		return linkFlairText;
	}

	public String getId() {
		return id;
	}

	public int getGilded() {
		return Integer.parseInt(gilded);
	}

	public String getSecureMediaEmbed() {
		return secureMediaEmbed;
	}

	public boolean getClicked() {
		return clicked.equals("true");
	}

	public boolean getStickied() {
		return stickied.equals("true");
	}

	public String getAuthor() {
		return author;
	}

	public String getMedia() {
		return media;
	}

	public int getScore() {
		return Integer.parseInt(score);
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public boolean getOver18() {
		return over18.equals("true");
	}

	public boolean getHidden() {
		return hidden.equals("true");
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public String getSubredditID() {
		return subredditID;
	}

	public boolean getEdited() {
		return edited.equals("true");
	}

	public String getLinkFlairCSSClass() {
		return linkFlairCSSClass;
	}

	public String getAuthorFlairCSSClass() {
		return authorFlairCSSClass;
	}

	public int getDowns() {
		return Integer.parseInt(downs);
	}

	public boolean getSaved() {
		return saved.equals("true");
	}

	public boolean getIsSelf() {
		return isSelf.equals("true");
	}

	public String getPermalink() {
		return permalink;
	}

	public String getName() {
		return name;
	}

	public String getCreated() {
		return created;
	}

	public String getUrl() {
		return url;
	}

	public String getAuthorFlairText() {
		return authorFlairText;
	}

	public String getCreatedUTC() {
		return createdUTC;
	}

	public int getUps() {
		return Integer.parseInt(ups);
	}

	public int getNumComments() {
		return Integer.parseInt(numComments);
	}
	
	public String getTitle(){
		return this.title;
	}

	public boolean getVisited() {
		return visited.equals("true");
	}

	public int getNumReports() {
		return Integer.parseInt(numReports);
	}

	public String getDistinguished() {
		return distinguished;
	}

	private String domain;
	private String bannedBy;
	private String mediaEmbed;
	private String subreddit;
	private String selfTextHTML;
	private String selfText;
	private String likes;
	private String secureMedia;
	private String linkFlairText;
	private String id;
	private String gilded;
	private String secureMediaEmbed;
	private String clicked;
	private String stickied;
	private String author;
	private String media;
	private String score;
	private String approvedBy;
	private String over18;
	private String hidden;
	private String thumbnail;
	private String subredditID;
	private String edited;
	private String linkFlairCSSClass;
	private String authorFlairCSSClass;
	private String downs;
	private String saved;
	private String isSelf;
	private String permalink;
	private String name;
	private String created;
	private String url;
	private String authorFlairText;
	private String title;
	private String createdUTC;
	private String ups;
	private String numComments;
	private String visited;
	private String numReports;
	private String distinguished;
	
	public Post(JSONObject post){
		this.title = post.get("domain") == null ? "null" : post.get("domain").toString() ;
		this.bannedBy = post.get("banned_by") == null ? "null" : post.get("banned_by").toString();
		this.mediaEmbed = post.get("media_embed") == null ? "null" : post.get("media_embed").toString();
		this.subreddit = post.get("subreddit") == null ? "null" : post.get("subreddit").toString();
		this.selfTextHTML = post.get("selftext_html") == null ? "null" : post.get("selftext_html").toString();
		this.selfText = post.get("selftext") == null ? "null" : post.get("selftext").toString();
		this.likes = post.get("likes") == null ? "null" : post.get("likes").toString();
		this.secureMedia = post.get("secure_media") == null ? "null" : post.get("secure_media").toString();
		this.linkFlairText = post.get("link_flair_text") == null ? "null" : post.get("link_flair_text").toString();
		this.id = post.get("id") == null ? "null" : post.get("id").toString();
		this.gilded = post.get("gilded") == null ? "null" : post.get("gilded").toString();
		this.secureMediaEmbed = post.get("secure_media_embed") == null ? "null" : post.get("secure_media_embed").toString();
		this.clicked = post.get("clicked") == null ? "null" : post.get("clicked").toString();
		this.stickied = post.get("stickied") == null ? "null" : post.get("stickied").toString();
		this.author = post.get("author") == null ? "null" : post.get("author").toString();
		this.media = post.get("media") == null ? "null" : post.get("media").toString();
		this.score = post.get("score") == null ? "null" : post.get("score").toString();
		this.approvedBy = post.get("approved_by") == null ? "null" : post.get("approved_by").toString();
		this.over18 = post.get("over18") == null ? "null" : post.get("over18").toString();
		this.hidden = post.get("hidden") == null ? "null" : post.get("hidden").toString();
		this.thumbnail = post.get("thumbnail") == null ? "null" : post.get("thumbnail").toString();
		this.subredditID = post.get("subreddit_id") == null ? "null" : post.get("subreddit_id").toString();
		this.edited = post.get("edited") == null ? "null" : post.get("edited").toString();
		this.linkFlairCSSClass = post.get("link_flair_css_class") == null ? "null" : post.get("link_flair_css_class").toString();
		this.authorFlairCSSClass = post.get("author_flair_css_class") == null ? "null" : post.get("author_flair_css_class").toString();
		this.downs = post.get("downs") == null ? "null" : post.get("downs").toString();
		this.saved = post.get("saved") == null ? "null" : post.get("saved").toString();
		this.isSelf = post.get("is_self") == null ? "null" : post.get("is_self").toString();
		this.permalink = post.get("permalink") == null ? "null" : post.get("permalink").toString();
		this.name = post.get("name") == null ? "null" : post.get("name").toString();
		this.created = post.get("created") == null ? "null" : post.get("created").toString();
		this.url = post.get("url") == null ? "null" : post.get("url").toString();
		this.authorFlairText = post.get("author_flair_text") == null ? "null" : post.get("author_flair_text").toString();
		this.title = post.get("title") == null ? "null" : post.get("title").toString();
		this.createdUTC = post.get("created_utc") == null ? "null" : post.get("created_utc").toString();
		this.ups = post.get("ups") == null ? "null" : post.get("ups").toString();
		this.numComments = post.get("num_comments") == null ? "null" : post.get("num_comments").toString();
		this.visited = post.get("visited") == null ? "null" : post.get("visited").toString();
		this.numReports = post.get("num_reports") == null ? "null" : post.get("num_reports").toString();
		this.distinguished = post.get("distinguished") == null ? "null" : post.get("distinguished").toString();
	}
	
	public Post(String title){
		this.title = title;
	}
	
	@Override
	public boolean equals(Object post){
		
		if(getClass() != post.getClass())
			return false;
		
		return this.title.equals(((Post)post).title);
	}
	
	public ArrayList<Comment> getComments(){
		return getComments(Sort.CONFIDENCE);
	}
	
	public ArrayList<Comment> getComments(String sort){
		
		try{
			
			StringBuilder response = RequestFactory.getPostComments(this, sort);
			
			if(response == null)
				throw new Exception();
			
			JSONObject postCommentsResponse = (JSONObject)((JSONArray)new JSONParser().parse(response.toString())).get(1);
			postCommentsResponse = (JSONObject)new JSONParser().parse(postCommentsResponse.get("data").toString());
	        JSONArray comments = (JSONArray)new JSONParser().parse(postCommentsResponse.get("children").toString());
	        
	        Iterator<JSONObject> i = comments.iterator();
	        ArrayList<Comment> postComments = new ArrayList<Comment>();
	        
	        while(i.hasNext()){
	        	JSONObject currComment = (JSONObject)i.next();
	        	
	        	if(currComment.get("kind").toString().equals("t1")){
	        		currComment = (JSONObject)new JSONParser().parse(currComment.get("data").toString());
	        		postComments.add(new Comment(currComment));
	        	}
	        }
	        
	        return postComments;
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Unable to retrieve comments for post: " + title + " in " + subreddit);
		}
		
		return null;
		
	}
	
	@Override
	public String toString(){
		return "Domain: " + this.domain + "\n" +
				"Banned by: " + this.bannedBy + "\n" +
				"Media embed: " + this.mediaEmbed + "\n" +
				"Subreddit: " + this.subreddit + "\n" +
				"Self text HTML: " + this.selfTextHTML + "\n" +
				"Self text: " + this.selfText + "\n" +
				"Likes: " + this.likes + "\n" +
				"Secure media: " + this.secureMedia + "\n" +
				"Link flair text: " + this.linkFlairText + "\n" +
				"ID: " + this.id + "\n" +
				"Gilded: " + this.gilded + "\n" +
				"Secure media embed: " + this.secureMediaEmbed + "\n" +
				"Clicked: " + this.clicked + "\n" +
				"Stickied: " + this.stickied + "\n" +
				"Author: " + this.author + "\n" +
				"Media: " + this.media + "\n" +
				"Score: " + this.score + "\n" +
				"Approved by: " + this.approvedBy + "\n" +
				"Over 18: " + this.over18 + "\n" +
				"Hidden: " + this.hidden + "\n" +
				"Thumbnail: " + this.thumbnail + "\n" +
				"Subreddit ID: " + this.subredditID + "\n" +
				"Edited: " + this.edited + "\n" +
				"Link flair CSS class: " + this.linkFlairCSSClass + "\n" +
				"Author flair CSS class: " + this.authorFlairCSSClass + "\n" +
				"Downs" + this.downs + "\n" +
				"Saved: " + this.saved + "\n" +
				"Is self: " + this.isSelf + "\n" +
				"Permalink: " + this.permalink + "\n" +
				"Name: " + this.name + "\n" +
				"Created: " + this.created + "\n" +
				"URL: " + this.url + "\n" +
				"Author flair text: " + this.authorFlairText + "\n" +
				"Title: " + this.title + "\n" +
				"Created UTC: " + this.createdUTC + "\n" +
				"Ups: " + this.ups + "\n" +
				"Num comments: " + this.numComments + "\n" +
				"Visited: " + this.visited + "\n" +
				"Num reports: " + this.numReports + "\n" +
				"Distinguished: " + this.distinguished + "\n";
	}

}

package com.rammit.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import com.rammit.entity.Post;
import com.rammit.entity.Subreddit;
import com.rammit.entity.User;
import com.rammit.util.Sort;

public class SubredditTests {

	private static User user;
	private static Subreddit subreddit;
	private static User nonLoggedInUser;

	@BeforeClass
	public static void setUp(){
		user = new User("rammittester", "ramseyiscool");
		subreddit = new Subreddit("/r/soccer/");
		nonLoggedInUser = new User("slavik262", "");
		user.connect();
	}
	
	@Test
	public void getSubredditPosts(){
		ArrayList<Post> subredditPosts = subreddit.getPosts(Sort.HOT);
		//System.out.println(subredditPosts);
		assertTrue(subredditPosts.size() != 0);
	}

}

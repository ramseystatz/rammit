package com.rammit.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import com.rammit.entity.Comment;
import com.rammit.entity.Subreddit;
import com.rammit.entity.User;
import com.rammit.util.Helpers;
import com.rammit.util.Sort;

public class PostTests {

	private static User user;
	private static Subreddit subreddit;
	private static User nonLoggedInUser;

	@BeforeClass
	public static void setUp(){
		user = new User("rammittester", "ramseyiscool");
		subreddit = new Subreddit("/r/soccer/");
		nonLoggedInUser = new User("slavik262", "");
		user.connect();
	}
	
	@Test
	public void getPostComments(){
		ArrayList<Comment> postComments = subreddit.getPosts().get(0).getComments(Sort.TOP);
		// Helpers.printCommentTree(postComments, 0);
		assertTrue(postComments.size() != 0);
	}

}

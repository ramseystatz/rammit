package com.rammit.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import com.rammit.entity.Comment;
import com.rammit.entity.Friend;
import com.rammit.entity.Post;
import com.rammit.entity.Subreddit;
import com.rammit.entity.User;
import com.rammit.entity.UserInfo;
import com.rammit.util.Helpers;
import com.rammit.util.Sort;

public class UserTests {
	
	private static User user;
	private static Subreddit subreddit;
	private static User nonLoggedInUser;

	@BeforeClass
	public static void setUp(){
		user = new User("rammittester", "ramseyiscool");
		subreddit = new Subreddit("/r/soccer/");
		nonLoggedInUser = new User("slavik262", "");
	}
	
	@Test
	public void login() {
		user.connect();
		//System.out.println(user.getModhash());
		assertTrue(user.getModhash() != null && user.getCookie() != null);
	}
	
	@Test
	public void getSubscribedSubreddits(){		
		ArrayList<Subreddit> userSubreddits = user.getSubscribedSubreddits();
		// System.out.println(userSubreddits);
		assertTrue(userSubreddits.contains(new Subreddit("/r/soccer/")));
	}
	
	@Test
	public void getNonLoggedInUserInfo(){
		UserInfo info = nonLoggedInUser.getUserInfo();
		//System.out.println(info);
		assertTrue(info.getUsername().equals("slavik262"));
	}
	
	@Test
	public void getLoggedInUserInfo(){
		UserInfo info = user.getUserInfo();
		//System.out.println(info);
		assertTrue(info.getUsername().equals("rammittester"));
	}
	
	@Test
	public void getUserPosts(){
		ArrayList<Post> userPosts = new User("checkenginelight", "").getUserPosts();
		//System.out.println(userPosts);
		assertTrue(userPosts.contains(new Post("Anyone know a good place to buy art?")));
	}
	
	@Test
	public void getUserComments(){
		ArrayList<Comment> userComments = new User("checkenginelight", "").getUserComments();
		System.out.println(userComments);
		assertTrue(userComments.contains(new Comment("Steady.")));
	}

	@Test
	public void getUserFriends(){
		ArrayList<Friend> userFriends = user.getUserFriends();
		//System.out.println(userFriends);
		assertTrue(userFriends.contains(new Friend("checkenginelight")));
	}
	
	@Test
	public void upvoteComment(){
		Comment comment = subreddit.getPosts().get(0).getComments(Sort.TOP).get(0);
		user.upvote(comment);
	}
	
	@Test
	public void upvotePost(){
		Post post = subreddit.getPosts(Sort.TOP).get(0);
		user.unvote(post);		
	}
	
}
